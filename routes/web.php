<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\InboxController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\PaketController;
use App\Http\Controllers\PesanController;
use Illuminate\Auth\Events\Login;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

//homePage
Route::get('/', [HomeController::class, 'index'])->name('paket');

//Routes Login & Register
Route::get('/login', [LoginController::class, 'login'])->name('login');
Route::post('/loginproses', [LoginController::class, 'loginProses'])->name('loginProses');
Route::get('/logout', [LoginController::class, 'logout'])->name('logout');

//Routes Admin
Route::get('/admin', [AdminController::class, 'admin'])->name('homeAdmin')->middleware('auth');
Route::get('/about', function(){
    return redirect('/admin');
});

//Routes Paket
Route::get('/paket', [PaketController::class, 'paket'])->name('paket');
Route::get('/hapuspaket/{id}', [PaketController::class, 'hapusPaket'])->name('hapusPaket');
Route::get('/tambahpaket', [PaketController::class, 'tambahpaket'])->name('tambahpaket');
Route::post('/insertpaket', [PaketController::class, 'insertpaket'])->name('inserpaket');
Route::get('/edit/{id}', [PaketController::class, 'editMenu'])->name('menuEdit');
Route::post('/editpaket/{id}', [PaketController::class, 'editPaket'])->name('editpaket');

//Routes Inbox
Route::get('/inbox', [PesanController::class, 'inbox'])->name('Inbox');
Route::get('/hapuspesan/{id}', [PesanController::class, 'hapusPesan'])->name('deleteInboxMessage');
Route::post('/pushInbox', [PesanController::class, 'pushPesan'])->name('pushInboxMessage');


