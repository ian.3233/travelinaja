<?php

namespace App\Http\Controllers;

use App\Models\paket;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    //

    public function index(){
        $datapaket = paket::all();
        // dd($data);
        return view('index',compact('datapaket'));
    }
}
