<?php

namespace App\Http\Controllers;

use App\Models\paket;
use Illuminate\Http\Request;

class PaketController extends Controller
{

    public function paket(){
        $datapaket = paket::all();
        // dd($data);
        return view('paket',compact('datapaket'));
    }
    
    public function tambahpaket(){
        return view('tambahpaket');
    }

    public function insertpaket(Request $request){

        $paket = new paket();
        $paket->nama = $request->nama;
        $paket->deskripsi = $request->deskripsi;
        $paket->save();
        return redirect()->route('paket')->with('success','Data Berhasil Ditambahkan');
    }

    public function hapusPaket($id){
        $datapaket = paket::find($id);
        $datapaket->delete();
        return redirect()->route('paket')->with('success','Data Berhasil Dihapus');
    }


    public function editMenu($id){
        $data = paket::find($id);
        // dd($datapaket);
        return view('editpaket',compact('data'));
    }

    public function editPaket(Request $request, $id){
        $datapaket = paket::find($id);
        $datapaket->nama = $request->nama;
        $datapaket->deskripsi = $request->deskripsi;
        $datapaket->save();

        return redirect()->route('paket')->with('success','Data Berhasil Diubah!!!');
    }


}
